# agilebpm-base-spring-boot

## 项目介绍

本项目旨在让 spring boot 项目 以比较简单的形式整合 AgileBPM 流程服务

#### 软件架构
软件架构说明

#### 使用说明

引入依赖

```xml
<dependency>
  <groupId>com.dstz</groupId>
  <artifactId>agilebpm-base-spring-boot-starter</artifactId>
  <version>0.0.1-SNAPSHOT</version>
</dependency>
```

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


# 还处于测试阶段，请耐心等待正式版
